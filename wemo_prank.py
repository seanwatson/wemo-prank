import datetime
import random
import sys
import threading
import time

from ouimeaux.environment import Environment
from ouimeaux.signals import receiver, statechange

START_TIME = 1455682818
DEBOUNCE_S = 3600
TIME_OF_LAST_TOGGLE = 0
LAST_STATE = 0


class ToggleThread(threading.Thread):

    def __init__(self, switch):
        super(ToggleThread, self).__init__()
        self.switch = switch

    def run(self):
        print str(datetime.datetime.now()), ": toggling"
        self.switch.toggle()


def main():

    env = Environment()
    env.start()
    print "starting"
    env.discover(10)
    print "discovery done"


    @receiver(statechange)
    def switch_toggle(sender, **kwargs):
        global TIME_OF_LAST_TOGGLE
        global LAST_STATE
        print ""
        print str(datetime.datetime.now()), ": switch toggled", str(sender), str(kwargs)
        if kwargs.get('state', 0) == LAST_STATE:
            print "state hasn't changed, ignoring"
            return
        LAST_STATE = kwargs.get('state', 0)
        if time.time() - TIME_OF_LAST_TOGGLE > DEBOUNCE_S:
            days = ((time.time() - START_TIME) / 86400)
            days = max(1, min(days, 36))
            print str(datetime.datetime.now()), ": outside of debounce time, chance of toggle:", str(days / 36 * 100), '%'
            if random.randint(0, 35) < days:
                print str(datetime.datetime.now()), ": passed random chance test, actually toggling"
                time.sleep(random.randint(1,3))
                TIME_OF_LAST_TOGGLE = time.time()
                t = ToggleThread(sender)
                t.start()


    try:
        env.wait()
    except (KeyboardInterrupt, SystemExit):
        sys.exit(0)


if __name__ == '__main__':
    main()
